from typing import List, Optional

from bson import ObjectId
from pydantic import BaseModel, Field


class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate
    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError("Invalid objectid")
        return ObjectId(v)
    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type="string")


class Provider(BaseModel):
    id: str
    provider: str


class ArticletModel(BaseModel):
    idMG: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    id: int = Field(default_factory=0)
    featured: Optional[bool] = Field(default_factory=False)
    title: str = Field(...)
    url: str = Field(...)
    imageUrl: str = Field(...)
    newsSite: str = Field(...)
    summary: str = Field(...)
    publishedAt: str = Field(...)
    launches: Optional[List[Provider]] = Field(default_factory=[])
    events: Optional[List[Provider]] = Field(default_factory=[])

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                    "id": 0,
                    "title": "title string",
                    "url": "https://www.google.com/",
                    "imageUrl": "https://img.com/",
                    "newsSite": "Sample.com",
                    "summary": "",
                    "publishedAt": "2021-09-16T22:00:00.000Z",
                    "updatedAt": "2021-05-18T13:43:22.539Z",
                    "featured": False,
                    "launches": [],
                    "events": []
            }
        }


class UpdateArticleModel(BaseModel):
    featured: Optional[bool]
    title: Optional[str]
    url: Optional[str]
    imageUrl: Optional[str]
    newsSite: Optional[str]
    summary: Optional[str]
    publishedAt: Optional[str]
    launches: Optional[List[Provider]]
    events: Optional[List[Provider]]

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "title": "title string",
                "url": "https://www.google.com/",
                "imageUrl": "https://img.com/",
                "newsSite": "Sample.com",
                "summary": "",
                "publishedAt": "2021-09-16T22:00:00.000Z",
                "updatedAt": "2021-05-18T13:43:22.539Z",
                "featured": False,
                "launches": [],
                "events": []
            }
        }
