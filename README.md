# FastAPI and MongoDB
API com artigos sobre voos baseado na [Spaceflight News API](https://spaceflightnewsapi.net/)\
\
[Preview](https://mohs6q.deta.dev/)\
[Documentation](https://mohs6q.deta.dev/docs)\
[Vídeo de Apresentação](https://www.loom.com/share/b805def5738f4aadb6670c529b3c4e1e)


> This is a challenge by [Coodesh](https://coodesh.com/)

## Tecnologias utilizadas
- Python
- FastAPI
- MongoDB
- APScheduler

## Como rodar

### Requisito

[Python](https://www.python.org/downloads/)

Versão usada no projeto: 3.10.1

### Criando um ambiente virtual
`
$python -m venv .venv
`

### Ativando o ambiente virtual
##### Linux
`
$source .venv/bin/activate
`

##### Windows
`
$.venv\Scripts\activate
`

### Instalando os pacotes
`
$pip install -r requeriments.txt
`

#### MongoDB_URL
- Crie um arquivo na mesma pasta com o nome env.py
- Inclua a url do mongodb:\
Ex:\
`
MONGODB_URL="mongodb+srv://<name>:<password>@cluster0.hrwdm.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
`

### Rodando a api

`
$uvicorn main:app
`
