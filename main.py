from datetime import datetime
from typing import List

import requests
from apscheduler.jobstores.mongodb import MongoDBJobStore
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from fastapi import Body, FastAPI, HTTPException, Response, status
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from motor import motor_asyncio
from pymongo import MongoClient

from env import MONGODB_URL
from models import ArticletModel, UpdateArticleModel

app = FastAPI(title='API Fastapi and MongoDB - News flight based')

client = motor_asyncio.AsyncIOMotorClient(MONGODB_URL)
db = client.newsflight

Schedule = None

#13851 last article
async def check_news_articles():
    try:
        last_id_api = requests.get('https://api.spaceflightnewsapi.net/v3/articles', params={"_limit": 1})
        last_id = await db['articles'].find().sort('id',-1).to_list(1)
        limit = last_id_api.json()[0]["id"] - last_id[0]['id']
        r = requests.get('https://api.spaceflightnewsapi.net/v3/articles', params={"_limit": limit, "id_gt":last_id[0]['id']})
        if r.json():
            result = await db["articles"].insert_many(r.json())
            return {"news articles request found": limit,"news articles inserted into the database": len(result.inserted_ids)}
        return {"message": "no news articles found", "news": limit}
    except HTTPException as err:
        raise HTTPException(status_code=err.status_code,detail=f"Error: {err}")
    except ValueError:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Error: Could not convert data to limit")
    except BaseException as err:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Error: Unexpected {err=}")
      


@app.on_event("startup")
async def load_schedule_or_create_blank():
    """
    Instatialise the Schedule Object as a Global Param and also load existing Schedules from SQLite
    This allows for persistent schedules across server restarts. 
    """
    global Schedule
    try:
        mongo_client = MongoClient(MONGODB_URL)
        jobstores = {
            'default': MongoDBJobStore(database='newsflight', collection='jobs', client=mongo_client)
        }
        Schedule = AsyncIOScheduler(jobstores=jobstores)
        schedule_check_news_articles = Schedule.add_job(check_news_articles, 'cron', hour=9, id=f"articles_{datetime.now()}", timezone="America/Sao_Paulo")
        Schedule.start()
        with open("log.txt", mode="a") as log:
            log.write(f"{datetime.now()} - Created Schedule Object: job_id {schedule_check_news_articles.id}\n")   
    except BaseException as err:
        with open("log.txt", mode="a") as log:
            log.write(f"{datetime.now()} - Unable to Create Schedule Object. Error: {err}\n")       


@app.on_event("shutdown")
async def pickle_schedule():
    """
    An Attempt at Shutting down the schedule to avoid orphan jobs
    """
    global Schedule
    try:
        Schedule.shutdown()
        with open("log.txt", mode="a") as log:
            log.write(f"{datetime.now()} - Disabled Schedule\n")
    except BaseException as err:
        with open("log.txt", mode="a") as log:
            log.write(f"{datetime.now()} - Error on Disabled Schedule: {err}\n")


@app.get('/', status_code=status.HTTP_200_OK)
async def home():
    return {'status':status.HTTP_200_OK, 'message':'Back-end Challenge 2021 🏅 - Space Flight News'}


@app.get('/articles/getalldata', status_code=status.HTTP_201_CREATED, summary="This drops the database and gets all the spaceflight news articles")
async def getalldata():
    try:
        count = requests.get('https://api.spaceflightnewsapi.net/v3/articles/count')
        r = requests.get('https://api.spaceflightnewsapi.net/v3/articles', params={"_limit": count})
        db["articles"].drop()
        result = await db["articles"].insert_many(r.json())
        return {"count articles request": int(count.content),"count inserted into the database": len(result.inserted_ids)}
    except HTTPException as err:
        raise HTTPException(status_code=err.status_code,detail=f"Error: {err}")
    except ValueError:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Error: Could not convert data to limit")
    except BaseException as err:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Error: Unexpected {err=}")
    

@app.get(
    "/articles", response_description="List all articles", response_model=List[ArticletModel]
)
async def list_articles(_skip: int = 0, _limit: int = 10):

    articles = await db["articles"].find().skip(_skip).limit(_limit).to_list(_limit)
    return list(articles)


@app.post("/articles", response_description="Add new article", response_model=ArticletModel, status_code=status.HTTP_201_CREATED)
async def create_article(article: ArticletModel = Body(...)):
    if (article_found := await db["articles"].find_one({"id": article.id})) is not None:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=f"Article {article_found['id']} already exists")
    else:
        article = jsonable_encoder(article)
        new_article = await db["articles"].insert_one(article)
        created_article = await db["articles"].find_one({"_id": new_article.inserted_id})
        return JSONResponse(status_code=status.HTTP_201_CREATED, content=created_article)



@app.get(
    "/articles/{id}", response_description="Get a single article", response_model=ArticletModel
)
async def show_article(id: int):
    if (article := await db["articles"].find_one({"id": id})) is not None:
        return article

    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Article {id} not found")


@app.put("/articles/{id}", response_description="Update an article", response_model=ArticletModel)
async def update_article(id: int, article: UpdateArticleModel = Body(...)):
    article = {k: v for k, v in article.dict().items() if v is not None}

    if len(article) >= 1:
        update_result = await db["articles"].update_one({"id": id}, {"$set": article})

        if update_result.modified_count == 1:
            if (
                updated_article := await db["articles"].find_one({"id": id})
            ) is not None:
                return updated_article

    if (existing_article := await db["articles"].find_one({"id": id})) is not None:
        return existing_article

    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Article {id} not found")


@app.delete("/articles/{id}", response_description="Delete an article", response_class=Response, status_code=status.HTTP_204_NO_CONTENT)
async def delete_article(id: int):
    delete_result = await db["articles"].delete_one({"id": id})

    if delete_result.deleted_count == 1:
        return Response(status_code=status.HTTP_204_NO_CONTENT)

    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Article {id} not found")
